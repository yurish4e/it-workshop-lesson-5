document.addEventListener('DOMContentLoaded', function() {
    topCity = ['Амстердам', 'Бобруйск', 'Рим', 'Ухань', 'Полтава'];
    topDishes = ['Борщ', 'Сало', 'Окрошка', 'Хлеб'];

    let cityNumber = document.querySelector('.cityNumber'),
        dishesNumber = document.querySelector('.dishesNumber');

    cityNumber.innerHTML = topCity.length;
    dishesNumber.innerHTML = topDishes.length;

   function showCity(myEl) {
       let element = document.querySelector(myEl);
       let ul = element.querySelector('ul');
       let myNum = element.querySelector('b');
       element.addEventListener('click', ()=>{
           if (ul.innerHTML == '') {
            for (let i = 0; i<topCity.length; i ++) {
                let li = document.createElement('li');
                li.innerHTML = topCity[i];
                ul.appendChild(li);
               }
               
           }
        element.classList.toggle('active');
       })
   } 

   function showDish(myEl) {
    let element = document.querySelector(myEl);
    let ol = element.querySelector('ol');
    element.addEventListener('click', ()=>{
        if (ol.innerHTML == '') {
         for (let i = 0; i<topDishes.length; i ++) {
             let li = document.createElement('li');
             li.innerHTML = topDishes[i];
             ol.appendChild(li);
            }
        }
     element.classList.toggle('active');
    })
} 
   showCity('.top_cities');
   showDish('.top_dishes');
})