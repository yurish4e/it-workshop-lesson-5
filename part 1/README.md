This is a repository of Jewish Culture IT workshop.
Here we will learn the front end, namely Angular.
It's our sandbox.

Each contributor:
* have own development branch - "NAME-dev"
* have to do each task in nested branch
* when task is done - create a merge request into "NAME-dev".