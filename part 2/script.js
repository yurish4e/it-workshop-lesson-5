document.addEventListener('DOMContentLoaded', function() {
    let nameFirst = document.querySelector('.name_first'),
        nameLast = document.querySelector('.name_last'),
        name = document.querySelector('.name'),
        packing = document.querySelector('.packing'),
        packingResult = document.querySelector('.packing_result'),
        stuffing = document.querySelector('.stuffing'),
        stuffList = document.querySelector('.stuff_list'),
        orderResult = document.querySelector('.order_result'),
        submit = document.querySelector('.submit');
    function stuff() {
        let stuffInput = stuffing.querySelectorAll('input'),
            total = document.querySelector('.total');
            totalPrice = 10;
            for (i = 0; i<stuffInput.length; i++) {
                if (stuffInput[i].checked) {
                    let stuffItem = document.createElement('p');
                    let price = 1.99;
                    if (stuffInput[i].value == 'cheese' || stuffInput[i].value == 'tomatoes') {
                        price = 3.99;
                    }
                    totalPrice += price; 
                    stuffItem.innerHTML = '- ' + stuffInput[i].value + ' $' + price;
                    
                    stuffList.appendChild(stuffItem);
                }
        }
        total.innerHTML = 'TOTAL ' + (totalPrice*0.95).toFixed(2) + '$';
    }    
    function submitOrder() {
        submit.addEventListener('click', function(){
            orderResult.classList.add('active');
            stuffList.innerHTML = '';
            stuff();
            name.innerHTML = nameFirst.value + ' ' + nameLast.value;
            packingResult.innerHTML = packing.value;
        })
    }
    submitOrder();
})